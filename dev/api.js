import express from 'express'
import bodyParser from 'body-parser'
import Blockchain from './blockchain.js'
import { v1 as uuid } from 'uuid'

const nodeAddress = uuid().split('-').join('')

const flipcoin = new Blockchain()
const app = express()

// parses raw data and url encoded data for use
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

//get entire blockchain
app.get('/blockchain', function (req, res) {
  res.send(flipcoin)
})

//create a new transaction
app.post('/transaction', function (req, res) {
  const blockIndex = flipcoin.createNewTransaction(
    req.body.amount,
    req.body.sender,
    req.body.recipient
  )
  res.json({ note: `Transaction will be added in block ${blockIndex}.` })
})

//mine a block or "create a new block"
app.get('/mine', function (req, res) {
  const lastBlock = flipcoin.getLastBlock()
  const previousBlockHash = lastBlock['hash']
  const currentBlockData = {
    transactions: flipcoin.pendingTransactions,
    index: lastBlock['index'] + 1,
  }
  const nonce = flipcoin.proofOfWork(previousBlockHash, currentBlockData)
  const blockhash = flipcoin.hashBlock(
    previousBlockHash,
    currentBlockData,
    nonce
  )

  //reward for mining
  flipcoin.createNewTransaction(12.5, '00', nodeAddress)

  const newblock = flipcoin.createNewBlock(nonce, previousBlockHash, blockhash)
  res.json({
    note: 'New block mined succesfully',
    block: newblock,
  })
})

app.listen(3000, function () {
  console.log('Listening on port 3000...')
})
